import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("========== Задание №1 ==========");
        System.out.print("Введите число 1, 2 или 3: ");
        Scanner if_switchSymbol = new Scanner(System.in);
        if (if_switchSymbol.hasNextInt()) {
            if_switch(if_switchSymbol.nextInt());
        }
        else System.out.println("Вы ввели не число");
        System.out.println("========== Задание №2 ==========");
        System.out.print("Вывод чисел в один ряд: ");
        oneLine();
        System.out.println("========== Задание №3 ==========");
        System.out.print("Введите число от 1 до 10: ");
        Scanner mulTableSymbol = new Scanner(System.in);
        if (mulTableSymbol.hasNextInt()) {
            multiplicationTable(mulTableSymbol.nextInt());
        }
        else System.out.println("Вы ввели не число");
        System.out.println("========== Задание №4 ==========");
        System.out.print("Введите размерность массива: ");
        Scanner randomArraySymbol = new Scanner(System.in);
        if (randomArraySymbol.hasNextInt()) {
            randomArray(randomArraySymbol.nextInt());
        }
        else System.out.println("Вы ввели не число");
    }

    public static void if_switch(int a){
        System.out.println("If");
        System.out.println("----------");
        if (a == 1){
            System.out.println("Вы ввели число 1");
        }
        else if (a == 2){
            System.out.println("Вы ввели число 2");
        }
        else if (a == 3){
            System.out.println("Вы ввели число 3");
        }
        else {
            System.out.println("Вы ввели не верное число");
        }
        System.out.println("__________");
        System.out.println("Switch");
        System.out.println("----------");
        switch (a){
            case (1):
                System.out.println("Вы ввели число 1");
                break;
            case (2):
                System.out.println("Вы ввели число 2");
                break;
            case (3):
                System.out.println("Вы ввели число 3");
            default:
                System.out.println("Вы ввели не верное число");
                break;
        }
    }
    public static void oneLine(){
        for (int i = 5; i > 0; i--) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
    public static void multiplicationTable(int x){
        for (int i = 1; i <= 10; i++) {
            System.out.println(x + "x" + i + "="+ x*i);
        }
    }
    public static void randomArray(int m){
        int [] ranArray = new int[m];
        Random rnd = new Random();
        for (int i = 0; i < ranArray.length; i++) {
            ranArray[i] = rnd.nextInt();
            System.out.println(ranArray[i]);
        }
    }








}